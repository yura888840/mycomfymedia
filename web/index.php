<?php
date_default_timezone_set('Europe/London');
require __DIR__.'/../vendor/autoload.php';
error_reporting(E_ALL);

$yii = __DIR__.'/../vendor/yiisoft/yii/framework/yii.php';
$config = __DIR__.'/../app/config/main.php';

defined('YII_DEBUG') or define('YII_DEBUG',true);
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

require_once($yii);
Yii::createWebApplication($config)->run();
