<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />

        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap-theme.min.css" />
        <link rel="stylesheet" type="text/css" href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" />

        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
<style>
body { background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAaCAYAAACpSkzOAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAA$
</style>

</head>

<body>
<div id="adv-menu-upload"></div>
  <div class="row">
    <div class="span12">
      <div class="head">
        <div class="row-fluid">
            <div class="span12">
                <div class="span6" id="logo">
                    <h1 class="muted"><?php echo CHtml::encode(Yii::app()->name); ?></h1>
                </div>

<?php if(Yii::app()->user->isGuest) { ?>
                <div class="span4 offset1" style="margin-top:10px;">
                    <button class="btn btn-info btn-large btn-block pull-right" type="button" id="btnregister">Registration</button>
                </div>
<?php } ?>
            </div>
        </div>

        <div class="navbar" id="mainmenu">
            <div class="navbar-inner" style="backgroud-color: #c0c0c0;">
                <div class="container">
                    <ul class="nav">
                    <li>
                        <a href="/site/front">Home</a>
                    </li>
                    <li>
                        <a href="/site/page?id=pricing">Pricing</a>
                    </li>
                    <li>
                        <a href="/site/page?id=packages">Packages</a>
                    </li>
                    <li>
                        <a href="/site/page?id=about">About Project</a>
                    </li>
                <?php if(!Yii::app()->user->isGuest) { ?>
                    <li>
                        <a href="/site/profile">My content</a>
                    </li>
                    <li>
                        <a href="/user/purchases/">My Purchases</a>
                    </li>
                    <li>
                        <a href="/user/payments/">Payments</a>
                    </li>
                    <li>
                        <a href="/user/payments/create">Create payment</a>
                    </li>
                    <li>
                        <a href="/site/logout/">Logout</a>
                    </li>
                <?php } ?>

                    </ul>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="row-fluid">
<iframe src="/site/pf" frameborder=1 seamless="seamless" width="100%" height="400">
</iframe>
</div>

        <?php //echo $content; ?>

<div class="clear"></div>


<br><br><br>
<div class="container">
<div class="row-fluid">
        <div class="span12">
            <div class="span2" style="width: 20%;">
                <ul class="unstyled">
                    <li>Company<li>
                    <li><a href="/site/page?id=aboutus">About us</a></li>
                </ul>
            </div>
            <div class="span2" style="width: 20%;">
                <ul class="unstyled">
                    <li>Ways to pay<li>
                    <li><a href="/site/page?id=pricing">Plans and pricing</a></li>
                </ul>
            </div>
            <div class="span2" style="width: 20%;">
                <ul class="unstyled">
                    <li>Resources<li>
                    <li><a href="/site/page?id=articles">Articles</a></li>
                    <li><a href="/site/page?id=partners">Partners</a></li>
                </ul>
            </div>
            <div class="span2" style="width: 20%;">
                <ul class="unstyled">
                    <li>Need help?<li>
                    <li><a href="/site/page?id=contactus">Contact us</a></li>
                </ul>
            </div>
        </div>
    </div>

    <hr>
    <div class="row-fluid">
        <div class="span12">
            <div class="span8">
                <a href="#">Terms of Service</a>
                <a href="#">Privacy</a>
                <a href="#">Security</a>
            </div>
            <div class="span4">
                <p class="muted pull-right">© 2013-2014 MyComfy. All rights reserved</p>
            </div>
        </div>
    </div>
</div>
</div><!-- page -->

<script data-main="/js/common" data-slug="mediabox-index" src="/js/require.js"></script>

</body>
</html>

