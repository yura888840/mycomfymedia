<style>
body {
	margin:0;
	padding:40px;
	background:#fff;
	font:80% Arial, Helvetica, sans-serif;
	color:#555;
	line-height:180%;
}

h1{
	font-size:180%;
	font-weight:normal;
	color:#555;
}
h2{
	clear:both;
	font-size:160%;
	font-weight:normal;
	color:#555;
	margin:0;
	padding:.5em 0;
}
a{
	text-decoration:none;
	color:#f30;	
}
p{
	clear:both;
	margin:0;
	padding:.5em 0;
}
pre{
	display:block;
	font:100% "Courier New", Courier, monospace;
	padding:10px;
	border:1px solid #bae2f0;
	background:#e3f4f9;	
	margin:.5em 0;
	overflow:auto;
	width:800px;
}

img{border:none;}
ul,li{
	margin:0;
	padding:0;
}
li{
	list-style:none;
	float:left;
	display:inline;
	margin-right:10px;
}



/*  */

#preview{
	position:absolute;
	border:1px solid #ccc;
	background:#333;
	padding:5px;
	display:none;
	color:#fff;
	}

/*  */
</style>

<!-- todo move to index. Gen index on this controller too -->
<div id="adv-menu-upload"></div>
<!-- todo  move to js loaders -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Title</h4>
      </div>
      <div class="modal-body">
        <img src="#" id="myModalImg"></img>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script src="https://netdna.bootstrapcdn.com/twitter-bootstrap/2.0.4/js/bootstrap-modal.js"></script>

<section class="container">

    <div id="splitter" class="row">
        <div class="span3" id="left-vertical">
    <div style="padding: 20px 0 0 20px;">
<?php if(Yii::app()->user->isGuest)$this->widget('application.modules.user.components.LoginWidget', array()); ?>

</div>

        </div>

        <div class="span9" id="vertical">
            <div id="fs-container">
                <div id="fs" class="fs-container-div">
<strong> TOP RATED </strong> <br />

<?php
$this->widget('bootstrap.widgets.TbThumbnails', array(
        'dataProvider'=> $data,
        'template'=>"{items}\n{pager}",
        'itemView'=>'_thumbs',
));
?>

                </div>
            </div>
        </div>


    </div>

</section>

<script type="text/javascript">
xOffset = 100;
yOffset = 30;

$(".thumb_img").hover(function(e){
        this.t = this.title;
        this.title = "";
	var v = $( this ).attr('c');console.log(v);
        var c = (this.t != "") ? "<br/>" + this.t : "";
        $("body").append("<p id='preview'><img src='http://storage.mycomfymediamarket.com/get/?s=s&id="+ v +"' alt='Image preview' />"+ c +"</p>");                               
        $("#preview")
                .css("top",(e.pageY - xOffset) + "px")
                .css("left",(e.pageX + yOffset) + "px")
                .fadeIn("fast");
},

function(){
        this.title = this.t;
        $("#preview").remove();
});

$(".thumb_img").mousemove(function(e){
        $("#preview")
                .css("top",(e.pageY - xOffset) + "px")
                .css("left",(e.pageX + yOffset) + "px");
});

</script>
