<?php
$content   = array (
  'isBrowserTriggerEnabled' => true,
  'lastTrackerCronRun' => 1415630958,
  'PrivacyManager.useAnonymizedIpForVisitEnrichment' => true,
  'PrivacyManager.ipAddressMaskLength' => 1,
  'PrivacyManager.doNotTrackEnabled' => true,
  'PrivacyManager.ipAnonymizerEnabled' => false,
  'currentLocationProviderId' => 'default',
  'CustomVariables.MaxNumCustomVariables' => 5,
);
$expires_on   = 1415631258;
$cache_complete   = true;
?>