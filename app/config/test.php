<?php

return CMap::mergeArray(
	require(dirname(__FILE__).'/main.php'),
	array(
		'components'=>array(
			'fixture'=>array(
				'class'=>'system.test.CDbFixtureManager',
			),

	                'db'=>array(
	                        'connectionString' => 'mysql:host=localhost;dbname=smm_test',
	                        'emulatePrepare' => true,
	                        'username' => 'root',
	                        'password' => 'sxKTzT7D6pkGmgmfMqkfMqk',
	                        'charset' => 'utf8',
	                ),
		),
	)
);
