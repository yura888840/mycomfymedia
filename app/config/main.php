<?php

Yii::setPathOfAlias('bootstrap', dirname(__FILE__).'/../extensions/bootstrap');
//Yii::setPathOfAlias("packages.redis", __DIR__."/../../vendor/codemix/yiiredis");
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'MyComfyMediamarket',
	'theme' => 'custom',
	'charset'=>'utf-8',

	'preload'=>array('log'),

	'import'=>array(
		'application.models.*',
		'application.components.*',

		'application.modules.user.models.*',
	),

	'modules'=>array(
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'123123',
	                'ipFilters'=>array('*'),
		),

		'user' => array(
			'debug' => false,
			'userTable' => 'user',
			'translationTable' => 'translation',
		),

	        'registration' => array(

	        ),

		'usergroup' => array(
			'usergroupTable' => 'usergroup',
			'usergroupMessageTable' => 'user_group_message',
		),

		'membership' => array(
			'membershipTable' => 'membership',
			'paymentTable' => 'payment',
		),

		'friendship' => array(
			'friendshipTable' => 'friendship',
		),

		'profile' => array(
			'privacySettingTable' => 'privacysetting',
			'profileFieldTable' => 'profile_field',
			'profileTable' => 'profile',
			'profileCommentTable' => 'profile_comment',
			'profileVisitTable' => 'profile_visit',
		),

		'role' => array(
			'roleTable' => 'role',
			'userRoleTable' => 'user_role',
			'actionTable' => 'action',
			'permissionTable' => 'permission',
		),

		'message' => array(
			'messageTable' => 'message',
		),

		'payments' => array(

		),

	),

	'components'=>array(

		'swiftMailer' => array(
		    'class' => 'ext.swiftMailer.SwiftMailer',
		),

		'user'=>array(
			'class' => 'application.modules.user.components.YumWebUser',
			'allowAutoLogin'=>true,
			'loginUrl' => array('//user/user/login'),
			//'registrationUrl' => array('//user/registration/registration'),
		),

		'cache' => array('class' => 'system.caching.CMemCache'),

	        'bootstrap'=>array(
	            'class'=>'bootstrap.components.Bootstrap',
	        ),

	        'authManager' => array(
	            'class' => 'PhpAuthManager',
	            'defaultRoles' => array('guest'),
	        ),

		'urlManager'=>array(
			'urlFormat'=>'path',
            		'showScriptName'=>false,
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
                                //'<controller:\w+>/<action:\w+>/<page:\w+>'=>'<controller>/<action>',

                		'api/items/<action>/<id:\d+>'=>'api/items/<action>',
			),
		),

		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=smm',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => 'sxKTzT7D6pkGmgmfMqkfMqk',
			'charset' => 'utf8',
		),

		'errorHandler'=>array(
			'errorAction'=>'site/error',
		),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),

	        'viewRenderer' => array(
	            'class' => 'ext.ETwigViewRenderer',
	            'fileExtension' => '.html',
	            'options' => array(
	                'autoescape' => true,
	            )
        	),

		'redis' => array(
			'class' => 'ARedisConnection',
			'hostname' => 'localhost',
			'port' => 6379,
			'database' => 1,
			'prefix' => 'Yii.redis.'
		),

	),

	'params'=>array(
	        'config' => array(
	            'session_limit' => 3600,
	            'session_long_limit' => 2592000,
	            'storage' => 'http://storage.mycomfymediamarket.com'
	        ),

	        'mediaTypes' => array(
	            'image' => '/img/mediabox/ftypes/image.png',
	            'doc' => '/img/mediabox/ftypes/msword.png',
	            'pdf' => '/img/mediabox/ftypes/pdf.png',
	            'txt' => '/img/mediabox/ftypes/text.png',
	            'exe' => '/img/mediabox/ftypes/executable.png',
	            'xls' => '/img/mediabox/ftypes/excel.png',
	            'audio' => '/img/mediabox/ftypes/audio.png',
        	    'html' => '/img/mediabox/ftypes/html.png',
	            'zip' => '/img/mediabox/ftypes/compress.png',
	            'video' => '/img/mediabox/ftypes/flash.png',
	            'any' => '/img/mediabox/ftypes/unknown.png',
	            'folder' => '/img/mediabox/ftypes/folder.png'
        	),

	        'extension' => array(
	            array('image', 'bmp', 'jpg', 'jpeg', 'gif', 'png'),
	            array('audio', 'ogg', 'mp3', ),
	            array('video', 'mp4', 'mov', 'wmv', 'flv', 'avi', 'mpg', '3gp', 'ogv', 'webm'),
	            array('text', 'txt'),
	            array('doc', 'doc', 'rtf', 'docx'),
	            array('pdf', 'pdf', 'djvu'),
	            array('txt', 'txt', 'lst', 'ini'),
	            array('exe', 'exe', 'com',' bat', 'sh'),
	            array('xls', 'xls', 'xlsx'),
	            array('html', 'htm', 'html', 'shtml'),
	            array('zip', 'zip', 'rar', 'tar', 'gz', '7z')
        	),

	        'mimetypes' => array(
	            'ogv' => 'video/ogg',
	            '3gp' => 'video/3gpp'
	        ),

	        'payments' => array(
	                'liqpay' => array(
	
	                ),

	                'pb' => array(
				'merchantId' => 'I01101VK',
				'aquireId' => 'eb8lb5suv83230s',
				'password' => '414963',

				'url' => 'https://ecommerce.liqpay.com/ecommerce/CheckOutPagen',
				'purchaseCurrency' => '980',
				'purchaseCurrencyExponent' => '2',
				'responseUrl' => 'http://mycomfymediamarket.com/user/payments/response',
        	        ),

			'privat24' => array(

				'merchantId' => '102549',
				'url' => 'https://api.privatbank.ua:9083/p24api/ishop',
				'responseUrl' => 'http://mycomfymediamarket.com/user/payments/response',
			),

        	        'wm' => array(

	                ),

        	),

		'adminEmail' => 'noreply@mycomfymediamarket.com',
		'columnsPerPage' => 20,

	),
);

