<?php
	
class SiteController extends Controller
{
    public function accessRules()
    {
        return array(
            array('allow',
                'actions'=>array('index', 'login', 'error', 'front', 'about'),
                'users'=>array('*'),
            ),
            array('allow',
                'actions'=>array('logout', 'profile', 'pf', 'con'),
                'users'=>array('@'),
            ),
            array('deny',
                'users'=>array('*'),
            ),
        );
    }

    public function beforeAction($action) 
    {
        Init::vars();

    	return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        if (Yii::app()->user->id) {
		$this->actionFront();
        } else {
            $this->actionLogin();
        }
    }

    public function actionPage()
    {
            $page = Yii::app()->request->getParam('id');
            $this->layout = 'layout_static';
            try
            {
                    $this->render('pages/' . $page);
            } catch (Exception $e) {
                    $this->redirect('404');
            }

    }

    public function actionProfile() 
    {
        if (Yii::app()->user->id) {
                        $this->layout = 'layout_static2';
                        $this->render('index_tt');
        } else {
            $this->actionLogin();
        }

    }

    public function actionPf() 
    {
            $this->layout = 'layout_static1';
            $this->render('index_tt');
    }

    public function actionCon() 
    {
            $this->layout = 'layout_static2';
            $this->render('index_tt');
    }

    public function actionError()
    {
            if($error=Yii::app()->errorHandler->error)
            {
                    if(Yii::app()->request->isAjaxRequest)
                            echo $error['message'];
                    else
                            //$this->render('error', $error);
                            $this->redirect('/');
            }
    }

    public function actionFront() 
    {
            $this->render('front', array('data' => $this->getRatedPics()) );
    }

    private function getRatedPics()
    {
            $model = YumSelectedFiles::model();
            $model->unsetAttributes();
            $d = $model->search();

            $rates = YumFileRates::model();

            return $d;
    }

    public function actionAbout()
    {
        $data = array();

        $this->render('about', $data);
    }

    public function actionLogin()
    {
        $model=new LoginForm;

        $error = false;

        if(isset($_POST['LoginForm']))
        {
            $model->attributes=$_POST['LoginForm'];
            if($model->validate() && $model->login()) {
                $this->redirect("/");
            } else {
                $error = true;
            }
        }
        $this->render('front', array('model'=>$model, 'error'=>$error, 'data' => $this->getRatedPics() ));
    }

    public function actionLogout()
    {
        Yii::app()->user->logout();

        $this->redirect(Yii::app()->homeUrl);
    }

}
