<?php

class UsersAdminController extends BaseController
{

	private $model;
	//@todo сверить статусы, с тем, что уже есть
	protected static $statuses = array(
		'WAITING_FOR_APPROVAL'	=> 0,
		'ACCEPTED' 		=> 1,
		'BLOCKED'		=> 2,
		'RESERVED'		=> 5
	);

	public static function statusFor($key = null) {
		if( !empty($key)
		    && array_key_exists($key, self::$statuses) )
		return self::$statuses[$key];

		return null;
	}

	public function actionIndex() {
		$this->render('index');
	}

	//@todo добавить, фильтра
	public function actionList() {
		$this->model = Users::model();

		
		
		$data = $this->model->findByAttributes();
		
		$this->render('list');

	}

	public function actionUserApprove() {

	}

	// добавить статус - блокирован
	public function actionUserSetStatus($status ) {
		if(!$this->isAdmin)
			Yii::app()-> exit();

		$id = $_GET['id'];
		$user = Users::model()->findByPk($id);

		$user->status = $status;
		$user->validate();

		$user->save();
	}

	//public function 

	// AJAX

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}
