<?php

class ItemsController extends Controller
{
	public function accessRules()
	{
        	return array(
        	    array('allow',
           		 'actions'=>array('index', 'list', 'add', 'remove', 'inc', 'dec'),
                	 'users'=>array('@'),
            	    ),
            	    array('deny',
            	        'users'=>array('*'),
            	    ),
        	);
        }

	protected $items;

	public function __construct() 
	{
		$this->items = new BasketItems();
		return parent::__construct($this->id, $this->module);
	}

	public function actionIndex()
	{ 

		$items = array();
		$res = array('success' => true, 'items' => $items);
		$error['message'] = 'This endpoint acept only POST requests';


                if(!Yii::app()->request->isAjaxRequest)
                	echo $error['message'];
                else
			echo json_encode($res);

	}


	public function actionAdd()
	{
		$success = true;
		$scu = $this->parseRequest();

		if(is_numeric($scu))
		{
			// @todo get Data for sku here
			$object = Files::model()->findByPk($scu);

			$data = !empty($object) /*&& property_exists($object, 'name')*/ ? $object->name: "Empty value";
			if(!empty($object)) $res  = $this->items->addItem($scu, $data);
				else 
				{
					$res = "This item doesn't exists in DB";
					$success = false;
				}
		} else 
			$success = false;

		$this->jsonResponse($res, $success);
	}


	public function actionRemove()
	{
		$scu = $this->parseRequest();

		$res  = $this->items->removeItems($scu);

		$this->jsonResponse($res);
	}

//@todo check if item exists in hash
        public function actionInc()
        {
                $scu = $this->parseRequest();

                $res  = $this->items->incItemCount($scu);

                $this->jsonResponse($res);
        }

        public function actionDec()
        {
                $scu = $this->parseRequest();

                $res  = $this->items->decItemCount($scu);

                $this->jsonResponse($res);
        }


	public function actionList()
	{
		$this->jsonResponse(true);
	}

	/**
	 * Get parameter from request
	 *
	 * @return $id integer
	 */
	private function parseRequest()
	{
                $id = (int) Yii::app()->request->getQuery('id');
                if(empty($id)) $scu = (int)Yii::app()->request->getPost('id');

		if(empty($id)) $id = "Needed param id is incorrect or nor present";

		return $id;
	}

	/**
	 * Json Response
	 * @var $res mixed true or error message
	 *
	 */
	private function jsonResponse($res, $success = true)
	{
                if($res && $success)
                {
                        $success = true;
                        $items = $this->items->getAllItems();
                } else {
                        $success = false;
                        $items = $res;
                }

                header('Content-type: application/json');

                echo json_encode(array('success' => $success, 'items' => $items) );
	}

	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
		}
	}

}
