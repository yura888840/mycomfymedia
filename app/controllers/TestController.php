<?php

class TestController extends Controller
{
    public function accessRules()
    {
        return array(
            array('allow',
                'actions'=>array('index', 'login', 'error', ),
                'users'=>array('*'),
            ),
            array('allow',
                'actions'=>array('logout'),
                'users'=>array('@'),
            ),
            array('deny',
                'users'=>array('*'),
            ),
        );
    }

    public function beforeAction($action) {//echo '**'; die();
        Init::vars();

    	return parent::beforeAction($action);
    }

	public function actionIndex()
	{ 
var_dump(Yii::app()->user->data()->id);
$a = Yii::app()->user->data()->id;

   $key = 'The Line Secret Key';
   $text = $a;
   $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
   $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
   $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $text, MCRYPT_MODE_ECB, $iv);
   var_dump( trim(base64_encode($crypttext)) );

//var_dump(Yii::app()->user->can("user_create")/*YumUser::model()->find('username = "demo"')->can('message_write')*/) ; 
//var_dump(Yii::app()->user->isAdmin());
//var_dump(YumUser::model()->findAll());
//die("**"); exit();
        if (Yii::app()->user->id) {
            $this->render('index');
        } else {
            $this->actionLogin();
        }
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

    /**
     * Displays the login page
     */
    public function actionLogin()
    {//echo '**';die();
        $model=new LoginForm;

        $error = false;

        if(isset($_POST['LoginForm']))
        {
            $model->attributes=$_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if($model->validate() && $model->login()) {
                $this->redirect("/");
            } else {
                $error = true;
            }
        }
        // display the login form
        $this->render('login', array('model'=>$model, 'error'=>$error));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
        Yii::app()->user->logout();

        $this->redirect(Yii::app()->homeUrl);
    }
}
