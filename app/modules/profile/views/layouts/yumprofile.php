<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="icon" type="image/x-icon" href="/favicon.ico">

    <!-- the styles -->
    <link href="/css/xvid.css" rel="stylesheet">
    <link href="/css/mediabox/mediabox.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="/js/html5.js"></script>
    <![endif]-->

    <!--[if lt IE 8]>
    <script src="/js/json2.js"></script>
    <![endif]-->
    <title>My Comfy MediaMarket</title>

    <!-- {{ App.bootstrap.register() }} -->
    <script type="text/javascript" src="/js/libs/jquery.cookie.js"></script>
<script type="text/javascript">
function getCookie(name) {
  var value = "; " + document.cookie;
  var parts = value.split("; " + name + "=");
  if (parts.length == 2) return parts.pop().split(";").shift();
} </script>
</head>
<body>

<div id="xvidbar" class="metanav navbar">
    <div class="navbar-inner">
        <div class="container">

            <ul class="nav pull-right">
                    <li>
                        <a href="/">Home</a>
                    </li>
                    <li>
                        <a href="/about">Help</a>
                    </li>
            </ul>
        </div>
    </div>
</div>

<div class="page">

<nav id="mainnav">
<div class="container">
<h1 class="xvidMediaBox_25px h1logo">My Comfy Media Market</h1>
</div>
</nav>

<div class="form container">
<div style="padding: 20px 0 0 20px;">


<?php
Yii::app()->clientScript->registerCssFile(
                Yii::app()->getAssetManager()->publish(
                        Yii::getPathOfAlias('YumAssets').'/css/yum.css'));

$module = Yii::app()->getModule('user');
$this->beginContent($module->baseLayout); ?>

<div id="usermenu">
<?php Yum::renderFlash(); ?>
<?php
if(Yum::hasModule('message')) {
        Yii::import('application.modules.message.components.*');
        $this->widget('MessageWidget');
}
if(Yum::hasModule('profile') && Yum::module('profile')->enableProfileVisitLogging) {
        Yii::import('application.modules.profile.components.*');
        $this->widget('ProfileVisitWidget');
}
$this->renderMenu(); ?>

</div>

<div id="usercontent">
<?php echo $content;  ?>
</div>

<?php $this->endContent(); ?>



</div>
</div>

</div>

<footer id="pageFooter" class="container">
    MyComfyMediaMarket 2014
</footer>

</body>
</html>

