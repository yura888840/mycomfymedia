<?

//Yii::import('application.modules.user.controllers.YumController');
class YumAdmin1Controller extends YumController {
	public $defaultAction = 'index'; //'login';

	/// переписать, access - ы
	public function accessRules() {
		return array(
				array('allow',
					'actions'=>array('index', 'view', 'login'),
					'users'=>array('*'),
					),
				array('allow',
					'actions'=>array('profile', 'logout', 'changepassword', 'passwordexpired', 'delete', 'browse'),
					'users'=>array('@'),
					),
				array('allow',
					'actions'=>array('admin','delete','create','update', 'list', 'assign', 'generateData', 'csv'),
					'expression' => 'Yii::app()->user->isAdmin()'
					),
				array('allow',
					'actions'=>array('create'),
					'expression' => 'Yii::app()->user->can("user_create")'
					),
				array('allow',
					'actions'=>array('admin'),
					'expression' => 'Yii::app()->user->can("user_admin")'
					),
				array('deny',  // deny all other users
						'users'=>array('*'),
						),
				);
	}



	public function actionIndex() {
		// If the user is not logged in, so we redirect to the actionLogin,
		// which will render the login Form

		$this->layout = Yum::module()->adminLayout;

		/*if(Yii::app()->user->isGuest)
			$this->actionLogin();
		else
			$this->actionList();*/
		$model = new YumAdminContent('search');
		$model->unsetAttributes();

		$this->render('contentadmin1', array('model' => $model, 'dataProvider' => $model->search()) ); exit();
		$_data = YumAdminContent::model()->getData(); 
		//var_dump($_data);
		foreach($_data as $k => &$v) {$d = $v['data'];
			$v['data'] = "<img src=\"data:image;base64," . $v['data']  . "\"/>";
		}

		$this->render( 'contentadmin', array('data' => $_data) );
	}

	/// стату - переделать
	public function actionStats() {
		$this->redirect($this->createUrl('/user/statistics/index'));
	}


	public function actionLogin() {
		// Do not show the login form if a session expires but a ajax request
		// is still generated
		if(Yii::app()->user->isGuest && Yii::app()->request->isAjaxRequest)
			return false;
		$this->redirect(array('/user/auth'));
	}

	public function actionLogout() {
		$this->redirect(array('//user/auth/logout'));
	}

	public function beforeAction($event) {
		if(!Yii::app()->user instanceof YumWebUser)
			throw new CException(Yum::t('Please make sure that Yii uses the YumWebUser component instead of CWebUser in your config/main.php components section. Please see the installation instructions.'));
		if (Yii::app()->user->isAdmin())
			$this->layout = Yum::module()->adminLayout;
		else
			$this->layout = Yum::module()->layout;
		return parent::beforeAction($event);
	}

	public function actionUpdate() {
		$file_id = intval($_GET['id']);

		if(empty($file_id)) $this->redirect('/user/admincontent1');


		$content_obj = YumAdminContent::model()->findByPk( $file_id);
		$attrs = array('is_active' => 1, 'is_new' => 0);
		$content_obj->setAttributes($attrs );
		$content_obj->save();
		$this->redirect('/user/admincontent1');
	}

	/**
	 * 
	 */
	public function actionDelete($id = null) {
                $file_id = intval($_GET['id']);

                if(empty($file_id)) $this->redirect('/user/admincontent1');

                $content_obj = YumAdminContent::model()->findByPk( $file_id);
                $attrs = array('is_active' => 1, 'is_new' => 0, 'trash' => 1);
                $content_obj->setAttributes($attrs );
                $content_obj->save();
                $this->redirect('/user/admincontent1');

		
	}


	public function actionAdmin()
	{
		if(Yum::hasModule('role'))
			Yii::import('application.modules.role.models.*');

		$this->layout = Yum::module()->adminLayout;

		$model = new YumUser('search');

		if(isset($_GET['YumUser']))
			$model->attributes = $_GET['YumUser'];

		$this->render('admin', array('model'=>$model));
	}

}
