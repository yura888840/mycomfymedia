<?php

/**
 * End- points for payments
 *
 * @author yuri
 */

class YumPaymentsController extends YumController
{
	public function filters()
	{
		return array(
				'accessControl', // perform access control for CRUD operations
				);
	}

	public function accessRules()
	{
		return array(
                                array('allow',
                                        'actions'=>array('index', 'list', 'balance', 'create', 'pay', 'result', 'response'),
                                        'users'=>array('@'),
                                        ),

				array('deny',
					'users'=>array('*'),
					),
				);
	}

	public function actionIndex() {
		$this-> actionList();
	}

	/**
	 * List of all payments for user / admin
	 *
	 */
	public function actionList() {
		$data = array();

		$model = YumPayments::model();
		$isAdmin = Yii::app()->user->isAdmin();

		if( !$isAdmin );
			$model->user_id = Yii::app() ->user-> data() -> id;

		if($isAdmin) $payments = $model-> findAll();
			else $payments = $model-> findAllbyAttributes(
			array(), $condition = 'user_id = :user_id', $params= array( ':user_id' => Yii::app() ->user-> data() -> id) );

		$balance_user = Yii::app()->user->isAdmin() ? 0 : Yii::app()->user->data()->id;
		$balanceMoney = $model->balanceFor( $balance_user );

		//@todo refactor using scopes
		$creditsU = YumRatesBalance::model()->findByAttributes(array('user_id' => $balance_user));
		$credits = $creditsU?$creditsU->getCreditsCount(): 0;

		$model = new Yumpayments('search');
		$model->unsetAttributes();
		if(!$isAdmin) $model->setAttributes ( array('user_id' => Yii::app()->user->data()->id ));

		$dataProvider = new CActiveDataProvider('YumPayments', array(
		        'criteria' => array(
		            'with' => array('user', ),
		        ),

		        'pagination' => array(
		            'pageSize' => Yii::app()->params['columnsPerPage'],
		        ),
		    ));

		$this->render('list', array( 'balance' => $balanceMoney, 'dataProvider' => $model->search(), 'model' => $model, 'credits'=> $credits ) );
	}

	public function actionCreate() {
		$model = new YumPayment('search');
		$model->unsetAttributes();
		//if isAjax - performAjaxValidation ..

		if(isset($_POST['YumPayment']) /*&& $model->setAttributes($_POST['YumPayment'])->validate() */ )
		{
	        	$model->attributes=$_POST['YumPayment'];
			// @todo  $model->validate();
			    Yii::app()->user->setState('amount', floatval($_POST['YumPayment']['amount'] ) );
		            $this->redirect('pay');
		}

		$this->render('select_payment', array('model' => $model) );
	}

	protected function performAjaxValidation($model, $form)
	{
	    if(isset($_POST['ajax']) && $_POST['ajax']==='payment-form')
	    {
	        echo CActiveForm::validate($model);
	        Yii::app()->end();
	    }
	}

	/**
	 * Start payment
	 */ 
	public function actionPay($gateway = "Privat24") {

		$amount = floatval(Yii::app()->user->getState('amount' ) );
		if(empty($amount)) 
			$this->redirect('create');

                $order_id = Yii::app()->user->data() ->id . strtotime("now");

                $desc = "Users balance recharge. Order #" . $order_id;
                $params = array(
                        'amount' => $amount,
                        'order_id' => $order_id,
                        'desc' => $desc,
                );

		// @todo $order->attributes($params );
		$order = YumOrders::model();
		$order->user_id = Yii::app()->user->data() ->id;
		$order->amount = $amount;
		$order->desc = $desc;
		$order->data = $order_id;
		$order->isNewRecord = true;

		if( $order->validate() )
			$order->save();

                $class = "YumPayment" . $gateway;
		// may be, static ?
		$dataForPaymentFormPreparator = new $class($params);
		$paymentData = $dataForPaymentFormPreparator->getDetails();

		$formBuilderClass = "YumPaymentForm" . $gateway;
		$data['form'] = $formBuilderClass::build($paymentData );

		// set session, or state
		Yii::app()->session['payment_type'] 	= $gateway;
		Yii::app()->session['order_id']		= $order_id;
		Yii::app()->session['amount']		= $amount;

		$this->render('payment_form', array('data' => $data) );
	}


	/**
	 * Finish payment
	 */
	public function actionResult() {
		$data = $_POST;

                //$class_suffix = $this->getPaymentGWPrefix();
		$classSuffix	= Yii::app()->session['payment_type'];
		$orderId	= Yii::app()->session['order_id'];

		$data['order_id_mycomfy'] = $orderId;


                $backupData = array('get' => $_GET, 'post' => $_POST);
		$backupData = base64_encode( gzdeflate( serialize($backupData) ) );
                $resPayment = YumResultPaymentLog::model();
		$resPayment->id = null;
                $resPayment->data = $backupData;
                $resPayment->gateway_abbr = $classSuffix;
                $resPayment->user_id = Yii::app()->user->data()-> id;
                $resPayment->order_id = $orderId;
                $resPayment->timestamp = gmdate('U');
		$resPayment->isNewRecord= true;
                $resPayment->save();

		//@todo builder
                $class = "YumPaymentResponse" . $classSuffix;
                
                $responser = new $class($data);
		// 'success' => true/ false, 'items' => array(..)
                $responseArr = $responser->parse();

		Yii::app()->session->remove('payment_type');
		Yii::app()->session->remove('order_id');
		$this->render('response', array('data' => $responseArr) );
	}

	public function actionResponse() {
		$this->actionResult();
	}

	// @todo refactor for validation
	private function getPaymentGWPrefix ($default = "PB" ) {
                
		if(array_key_exists( 'payment_type', Yii::app()->session-> toArray() ) && 
		    in_array(Yii::app()->session['payment_type'], PaymentGateways::getList() ))
			$class_suffix = Yii::app()->session['payment_type'];

		else $class_suffix = $default;

		return $class_suffix;
	}

}
