<?php

/**
 * End- points for purchases
 *
 * @author yuri
 */

class YumPurchasesController extends YumController
{
	public function filters()
	{
		return array(
				'accessControl', // perform access control for CRUD operations
				);
	}

	public function accessRules()
	{
		return array(
                                array('allow',
                                        'actions'=>array('index', 'list', 'balance', 'create', 'pay', 'result', 'response'),
                                        'users'=>array('@'),
                                        ),

				array('deny',
					'users'=>array('*'),
					),
				);
	}

	public function actionIndex() {
		$this-> actionList();
	}

	/**
	 * List of all purchase for user
	 *
	 */
	public function actionList() {
		$data = array();

		$isAdmin = Yii::app()->user->isAdmin();

		$model = Purchases::model();
		if( !$isAdmin );
			$model->user_id = Yii::app() ->user-> data() -> id;

		if($isAdmin) $purchases = $model-> findAll();
			else $purchases = $model-> findAllbyAttributes(
			array(), $condition = 'user_id = :user_id', $params= array( ':user_id' => Yii::app() ->user-> data() -> id) );

		$model = new YumPuchases('search');
		$model->unsetAttributes();
		if(!$isAdmin) $model->setAttributes ( array('user_id' => Yii::app()->user->data()->id ));

		$dataProvider = new CActiveDataProvider('YumPurchases', array(
		        'criteria' => array(
		            'with' => array('user', ),
		        ),

		        'pagination' => array(
		            'pageSize' => Yii::app()->params['columnsPerPage'],
		        ),
		    ));

		$this->render('list', array( 'dataProvider' => $model->search(), 'model' => $model ) );
	}

	// start purchases with ids
	public function actionCreate() {
		$model = new YumPurchasesBasket('search');
		$model->unsetAttributes();
		//if isAjax - performAjaxValidation ..

		if(isset($_POST['YumPurchases']) /*&& $model->setAttributes($_POST['YumPayment'])->validate() */ )
		{
	        	$model->attributes=$_POST['YumPurchases'];
			// @todo  $model->validate();
		            $this->redirect('purchasespay');
		}

		$this->render('select_purchases', array('model' => $model) );
	}

	protected function performAjaxValidation($model, $form)
	{
	    if(isset($_POST['ajax']) && $_POST['ajax']==='payment-form')
	    {
	        echo CActiveForm::validate($model);
	        Yii::app()->end();
	    }
	}

	/**
	 * Start payment for purchase
	 */ 
	public function actionPurchasespay() {

		$user_id = Yii::app()->user->data() ->id;

                $order_id = $user_id . gmdate("U");

		$purchasesBasket = YumPurchasesBasket::model()->findByAttributes(array('user_id' => $user_id, 'active' => 1));

		$creditsAmount = $purchasesBasket->getTotalAmount();

                $desc = "Purchase for goods. Order #" . $order_id;

                $params = array(
                        'amount' => $amount,
                        'order_id' => $order_id,
                        'desc' => $desc
                );

		$crModel = YumRatesBalance::model();
		$credits = $crModel->getCreditsCount($user_id );

		$limit = YumUserLimits::model()->getLimitFor($user_id );
		$success = false; $msg = '';
		if( $credits - $creditsAmount < $limit) {
			exit('Not enough credits on balance or credit limit reached');
		} else {

			$purchaseBasket->approveWithSave();
			$crModel->findByAttributes(array('user_id' => $user_id));
			$crModel->addAmountWithSave($amount );

			$ids = $purchaseBasket->getIds();
			$purchaseBasket->orderDone();

			$success = true;

		}

		$this->render('purchase_result', array('data' => $data) );
	}


	/**
	 * Finish payment
	 */
	public function actionResult() {
		$data = $_POST;

                //$class_suffix = $this->getPaymentGWPrefix();
		$classSuffix	= Yii::app()->session['payment_type'];
		$orderId	= Yii::app()->session['order_id'];

		$data['order_id_mycomfy'] = $orderId;


                $backupData = array('get' => $_GET, 'post' => $_POST);
		$backupData = base64_encode( gzdeflate( serialize($backupData) ) );
                $resPayment = YumResultPaymentLog::model();
                $resPayment->data = $backupData;
                $resPayment->gateway_abbr = $classSuffix;
                $resPayment->user_id = Yii::app()->user->data()-> id;
                $resPayment->order_id = $orderId;
                $resPayment->timestamp = gmdate('U');
		$resPayment->isNewRecord= true;
                $resPayment->save();

		//@todo builder
                $class = "YumPaymentResponse" . $classSuffix;
                
                $responser = new $class($data);
		// 'success' => true/ false, 'items' => array(..)
                $responseArr = $responser->parse();

		Yii::app()->session->remove('payment_type');
		Yii::app()->session->remove('order_id');
		$this->render('response', array('data' => $responseArr) );
	}

	public function actionResponse() {
		$this->actionResult();
	}

	// @todo refactor for validation
	private function getPaymentGWPrefix ($default = "PB" ) {
                
		if(array_key_exists( 'payment_type', Yii::app()->session-> toArray() ) && 
		    in_array(Yii::app()->session['payment_type'], PaymentGateways::getList() ))
			$class_suffix = Yii::app()->session['payment_type'];

		else $class_suffix = $default;

		return $class_suffix;
	}

}
