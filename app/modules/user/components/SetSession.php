<?php

class SetSession {

	private static $key = "The Line Secret Key";

	private static $cookie_name = '_a';

	public static function run () {
		$a = Yii::app()->user->data() -> id;

		//setcookie(self::$cookie_name, self::encryptCookie($a) );
//print_r($_COOKIE); die();
                $cookie = new CHttpCookie(self::$cookie_name, self::encryptCookie($a));
                $cookie->expire = time() + (3600*24*30);
        	Yii::app()->request->cookies[self::$cookie_name] = $cookie;
	}

	private static function encryptCookie($value){

		if(!$value){return false;}

		$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
		$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
		$crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, self::$key, $value, MCRYPT_MODE_ECB, $iv);
		return trim(base64_encode($crypttext));
	}

}
