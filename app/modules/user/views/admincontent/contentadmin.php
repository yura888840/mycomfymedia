<?
$this->title=Yii::t('UserModule.user','Recently uploaded');
?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
        'dataProvider'=>$model->search(),
         'filter'=>$model,
	 'columns' => array(
                array(
                        'name'=>'Id',
                        'type'=>'raw',
                        'value'=>'CHtml::link(CHtml::encode($data->id), $data->id)',
                ),
                array(
                        'name'=>'Name',
                        'type'=>'raw',
                        'value'=>'CHtml::link(CHtml::encode($data->name), $data->name)',
                ),
                array(
                        'name'=>'user_id',
                        'type'=>'raw',
                        'value'=>'CHtml::link(CHtml::encode($data->user->username), "mailto:".$data->user->profile->email)',
                ),
                array(
                        'name'=>'file_id',
                        'type'=>'raw',
                        'value'=>'print (($data->image)?"<img src=\"data:image;base64," . $data->image->getAttribute("data") . "\"/>" :"No image" )',
                ),


                        array(
                                'class'=>'CButtonColumn',
				'template'=>'{delete}{view}{update}',
/*            'buttons'=>array (
                'delete_contact' => array (
                    'label'=>'Delete this contact',
                    //'imageUrl'=>Yii::app()->request->baseUrl.'/delete.png',
                    'url'=>'Yii::app()->createUrl("contact/delete", array("id"=>$data->id))',
                    'visible' => '1',
                    "options" => array(
                        "class" => "delete_contact"
                    )
                )
            ),*/

                        ),

/*
                array(
                        'data'=>'Data',
                        'type'=>'raw',
                        'value'=>3,//'CHtml::link(CHtml::encode($data->data), "mailto:".$data->data)',
                ),
*/

	)
) ); ?>
