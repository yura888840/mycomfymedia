<h2> <?php echo Yum::t('Select amount to load into your balance'); ?> </h2>

<div class="form">
<?php $form = $this->beginWidget('CActiveForm', array(
                        'id'=>'payment-form',
                        //'enableAjaxValidation'=>true,
			//// do not touch this comment until you know what are you really doing !
                        'enableClientValidation'=>true,
                        'focus'=>array($this,'payment_amount'),
                        ));
?>
<div class="row1">
    <?php echo $form->labelEx($model,'Amount'); ?>
    <?php echo $form->textField($model,'amount'); ?>
    <?php echo $form->error($model,'amount'); ?>
</div>
<br/>
        <div class="row1 submit">
                <?php echo CHtml::submitButton(Yum::t('Proceed to payment')); ?>
        </div>

<?php $this->endWidget(); ?>
</div><!-- form -->

