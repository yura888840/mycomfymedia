<?
/**
 * Payment form for privat24 - button element
 * 
 */

class YumPaymentFormPrivat24 extends CFormModel
{

	public static function build($data) {

		$form = '
			<form action="' . $data['url'] . '" method="post" style="margin: 0px; padding: 0px;">
				<input type="hidden" name="amt" value="' . $data['amt'] . '" />
				<input type="hidden" name="ccy" value="' . $data['ccy'] . '" />
				<input type="hidden" name="merchant" value="' . $data['merchant'] . '" />
				<input type="hidden" name="order" value="' . $data['order'] . '" />
				<input type="hidden" name="details" value="' . $data['details'] . '" />
				<input type="hidden" name="ext_details" value="' . $data['ext_details'] . '" />
				<input type="hidden" name="pay_way" value="' . $data['pay_way'] . '" />
				<input type="hidden" name="return_url" value="' . $data['return_url'] . '" />
				<input type="hidden" name="server_url" value="' . $data['server_url'] . '" />
				<button type="submit" style="border: 0px; background: none;"><img src="https://client-bank.privatbank.ua/p24/img/buttons/api_logo_1.gif"></button>
		        </form>';

		return $form;
	}

}
