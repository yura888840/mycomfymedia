<?php

/**
 *
 */
class YumSelectedFiles extends YumActiveRecord
{

	public $id, $file_id, $photo;

	protected $data;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'files';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(

		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			//// эти рейты, ныжно, над, для отраж, отображений, для - end- point usr
			//'rate' => array(self::HAS_ONE, 'YumFileRates' , 'file_id'),
			'ratess' => array(self::HAS_ONE,'YumRatesForUse', '', 'on' => 'ratess.user_id=t.user_id' ),
			'image' => array(self::HAS_ONE, 'Image', 'file_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
		);
	}

        public function search()
        {

                $criteria = new CDbCriteria;

                return new CActiveDataProvider(get_class($this), array(
				    'criteria' => array(
					'with' => array('ratess' => array(), 'image' => array() ),
					'together' => true,
                                        'condition' => 'is_active = 1 and trash=0',
					//'group' => 'ratess.user_id, ratess.user_id',
					//'having'=> 'ratess.rate',
					'order' => 'ratess.rates desc, ratess.user_id desc, rand()',
					//'having' => 'rand()',
				    ),

                                    'pagination' => array('pageSize' => Yum::module()->pageSize),
					
				    )
				);

        }

	public function afterFind() {
		$this->photo = 'data:image;base64,' . $this->image->data;
		//'http://storage.mycomfymediamarket.com/get/?id=' . $this->id;
		$model = YumFileRates::model();
		$model->file_id = $this->id;
		$model->find();

		return parent::afterFind();
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
