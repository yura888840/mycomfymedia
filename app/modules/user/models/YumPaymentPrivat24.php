<?

/**
 *  Class for payment via Privat24
 * 
 */

class YumPaymentPrivat24 extends YumPayment
{

	protected $params;

	// @todo validator for order_id, amount
	public function __construct($data = array() ) {
		if(!empty($data) && is_array($data))
			$this->params = $data;

	}

	public function getDetails() {
	        $Version='1.0.0';

		$this->params['amount'] = $this->reformatAmount($this->params['amount']);

		$this->params['order_id'] = array_key_exists('order_id', $this->params)? 
                                                $this->params['order_id']: 
                                                $Yii::app()->user->data()-> id . strtotime('now');

		$this->params['ccy'] = "UAH";
		$this->params['pay_way'] = "privat24";

		$data = array(
			'url' => Yii::app()->params['payments']['privat24']['url'],
			'amt' => $this->params['amount'],
			'ccy' => $this->params['ccy'],
                        'merchant' => Yii::app()->params['payments']['privat24']['merchantId'],
                        'order' => $this->params['order_id'],
                        'details' => $this->params['desc'],
                        'ext_details' => $this->params['desc'],
                        'pay_way' => $this->params['pay_way'],
                        'return_url' => Yii::app()->params['payments']['privat24']['responseUrl'],
                        'server_url' => Yii::app()->params['payments']['privat24']['responseUrl'],

		);

		return $data;
	}

    private function reformatAmount($amount) {

        return number_format($amount, 2, '.', '');
    }

}
