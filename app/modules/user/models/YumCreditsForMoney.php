<?php

class YumCreditsForMoney {

	private static $def = 'USD';

	private static $rates = array(
                'USD' => 100,

                'EUR' => 134,

		'UAH' => 8.4,
	);

/*
	private static $rangesForSums = array(
		[ [0, 30], [30, 100], [100, 500], [500, 0] ],
		[ [0, 20], [20, 80], [80, 420], [420, 0] ],
		[ [0, 300], [300, 1000], [1000, 5000], [5000, 0] ],
		

	);

	private static $coefsFromSums = array(
		[1. 40,  1.8, 2.2, 2.5],
		[1. 42,  1.85, 2.26, 2.59],
		[1. 37,  1.74, 2.18, 2.49],
		
	);
*/
        private static $rangesForSums = array(
                array( array(0, 30), array(30, 100), array(100, 500), array(500, 0) ),
                array( array(0, 20), array(20, 80), array(80, 420), array(420, 0) ),
                array( array(0, 300), array(300, 1000), array(1000, 5000), array(5000, 0) ),


        );

        private static $coefsFromSums = array(
                array(1.40,  1.8, 2.2, 2.5),
                array(1.42,  1.85, 2.26, 2.59),
                array(1.37,  1.74, 2.18, 2.49),

        );



	public static function getRatesCoefFor($name = 'USD') {
		return 
			array_key_exists($name, self::$rates) ?
					self::$rates[$name] :
					self::$rates[self::$def ];

	}

	public static function calcRates($amount, $currency) {
		$rr = static::$rates[$currency];
		$key = array_search($currency, array_keys(static::$rates) );
		if(!$key) $key = 0;

		$ranges = static::$rangesForSums[$key];
		$found = false;$s = -1;$max = sizeof($ranges);
		while(!$found && ++$s < $max ) {
			if($amount >= $ranges[$s][0] && $amount < $ranges[$s][1]) $found = true;
		}
		if(!$found) $s = $max - 1;

		return ceil(static::$coefsFromSums[$key][$s] * $amount);
	}

}
