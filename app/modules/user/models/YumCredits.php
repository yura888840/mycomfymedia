<?

/**
 * This is the model class for a Credits in Yum
 *
 * 
 */
class YumCredits extends YumActiveRecord
{
	public $file_id;
	public $rate;

	public function scopes() {
		return array(
			//'admin' => array( ),
			//'user' => array('condition' => 'user_id=' . intval(Yii::app()-> user->data()-> id),),
		);
	}

	public function tableName()
        {
                return 'rates_for_use';
        }


	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}


	public function afterDelete()
	{

		return parent::afterDelete();
	}

	public function search()
	{
		/*
		$criteria = new CDbCriteria;

		if (Yum::hasModule('profile') && $this->profile) {
			$criteria->with = array('profile');
			if (isset($this->email))
				$criteria->addSearchCondition('profile.email', $this->email, true);
			else if ($this->profile && $this->profile->email)
				$criteria->compare('profile.email', $this->profile->email, true);
		}

		// Show newest users first by default
		if (!isset($_GET['YumUser_sort']))
			$criteria->order = 't.createtime DESC';

		$criteria->together = false;
		$criteria->compare('t.id', $this->id, true);
		$criteria->compare('t.username', $this->username, true);
		$criteria->compare('t.status', $this->status);
		$criteria->compare('t.superuser', $this->superuser);
		$criteria->compare('t.createtime', $this->createtime, true);
		$criteria->compare('t.lastvisit', $this->lastvisit, true);

		return new CActiveDataProvider(get_class($this), array(
					'criteria' => $criteria,
					'pagination' => array('pageSize' => Yum::module()->pageSize),
					));

		*/

		
	}

	public function beforeValidate()
	{
		if ($this->isNewRecord) {

		}

		return true;
	}

	public function afterSave()
	{
		if ($this->isNewRecord) {

		}

		return parent::afterSave();
	}

	public function rules()
	{
		return array();
	}


	// possible relations are cached because they depend on the active submodules
	// and it takes many expensive milliseconds to evaluate them all the time
	public function relations()
	{ 
		return array(
			'rate' => array(self::BELONGS_TO, 'Files', 'file_id'),
		);
	  
	}



	public function getCreditsCount()
	{
		return $this->rate;
	}


}
