<?

/**
 * This is the model class for a Result Payments log
 *
 * 
 */
class YumResultPaymentLog extends YumActiveRecord
{
	public $user_id, $data, $gateway_abbr, $order_id, $timestamp;

	public function scopes() {
		return array(

		);
	}

	public function tableName()
        {
                return 'payment_resp_log';
        }


	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}


	public function afterDelete()
	{

		return parent::afterDelete();
	}

	public function search()
	{

		
	}

	public function beforeValidate()
	{
		if ($this->isNewRecord) {

		}

		return true;
	}

	public function afterSave()
	{
		if ($this->isNewRecord) {

		}

		return parent::afterSave();
	}

	public function rules()
	{
		return array();
	}


	public function relations()
	{ 
		return array(

		);
	  
	}

}
