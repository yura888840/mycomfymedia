<?php

/**
 * This is the model class for a AdminContent in Yum
 *
 * 
 */
class YumBalance extends YumActiveRecord
{

	public $user_id, $balance, $timestamp;

	public function tableName()
        {
                return 'balance';
        }

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}


	public function beforeValidate()
	{
		if ($this->isNewRecord) {

		}

		return true;
	}

	public function beforeSave() {

		return true; //parent::beforeSave();
	}

	public function afterSave()
	{
		if ($this->isNewRecord) {
			
		}

		return parent::afterSave();
	}

	public function rules()
	{
		return array( 
			array('user_id, balance, timestamp','safe', 'on' => 'insert' ),
		);
	}


	// possible relations are cached because they depend on the active submodules
	// and it takes many expensive milliseconds to evaluate them all the time
	public function relations()
	{
		return array(
			//'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}


}
