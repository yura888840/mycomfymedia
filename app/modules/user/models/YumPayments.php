<?

/**
 * This is the model class for a Payments in Yum
 *
 * 
 */
class YumPayments extends YumActiveRecord
{
	public $user_id;

	public function scopes() {
		return array(
			//'admin' => array( ),
			//'user' => array('condition' => 'user_id=' . intval(Yii::app()-> user->data()-> id),),
		);
	}

	public function tableName()
        {
                return 'payments';
        }


	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}


	public function afterDelete()
	{

		return parent::afterDelete();
	}


	public function search()
        {

                $criteria=new CDbCriteria;

                $criteria->compare('user_id', $this->user_id, true);
                //$criteria->compare('email', $this->email, true);
                //$criteria->compare('createtime', $this->createtime, true);
               
                /*$criteria->with =>array('subscription:isactiv'=>array('select'=>'{{subscription}}.*','joinType'=>'LEFT OUTER JOIN',
                             ),
                           ),
                       'together'=>true,
                       ),*/


		$sort = new CSort;
                /*$sort->defaultOrder = 'username ASC';
                $sort->attributes = array(
                    'username'=>'username',
                    'amount' => array(
                            'asc'=>'subscription.amount',
                            'desc'=>'subscription.amount DESC',
                        ),
                );*/


                return new CActiveDataProvider(get_class($this), array(
                        'criteria'=>$criteria,
                         'sort'=>$sort
                ));
        }

	public function beforeValidate()
	{
		if ($this->isNewRecord) {

		}

		return true;
	}

	public function afterSave()
	{
		if ($this->isNewRecord) {

		}

		return parent::afterSave();
	}

	public function rules()
	{
		return array(

			array('user_id', 'safe', 'on' => 'search'),
		);
	}


	// possible relations are cached because they depend on the active submodules
	// and it takes many expensive milliseconds to evaluate them all the time
	public function relations()
	{ 
		return array(
			'user' => array(self::BELONGS_TO, 'YumUser', 'user_id'),
			//'order' => array(self::BELONGS_TO, 'YumOrders', 'order_id'),
			// @todo check here
		);
	  
	}

	public function balanceFor($user_id = 0) {
		$where = (!empty($user_id)) ? " WHERE user_id = ". $user_id: "";
		$query = "SELECT sum(amount) FROM payments". $where;

		return sprintf("%01.2f", floatval($this->getDbConnection() ->createCommand($query)-> queryScalar()));

	}

	public function balanceOf($user_id = 0){return $this->balanceFor($user_id );}


}
