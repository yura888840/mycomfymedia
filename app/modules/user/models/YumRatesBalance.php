<?php

/**
 * This is the model class for user balance credits
 *
 *  @author yuri
 */
class YumRatesBalance extends YumActiveRecord
{

	public $rates;

	public $user_id, $balance, $timestamp;

	public $attributesHistory = array(
		'rates',
	);


	public function tableName()
        {
                return 'rates_on_balance';
        }

        public function scopes() {
                return array(
                        'admin' => array( ),
                        'user' => array('condition' => 'user_id=' . intval(Yii::app()-> user->data()-> id),),
                );
        }

	public function behaviors()
	{
        	return array(
	            'ActiveRecordHistoryBehavior'=>
	                'application.behaviors.ActiveRecordHistoryBehavior',
	        );
	}

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function beforeValidate()
	{
		if ($this->isNewRecord) {

		}

		return parent::beforeValidate();
	}

	public function afterFind() {
		$this->balance = $this->rates;

		return parent::afterFind();
	}

	public function beforeSave() {
		$this->rates = $this->balance;

		return parent::beforeSave();
	}

	public function afterSave()
	{
		if ($this->isNewRecord) {
			
		}

		return parent::afterSave();
	}

	public function rules()
	{
		return array( 
			array('user_id, balance, timestamp','safe', 'on' => 'insert' ),
		);
	}

	public function relations()
	{
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}


        public function getCreditsCount($user_id = 0)
        {
		if(!empty($user_id)) {
			$this->user_id = $user_id;
			$this->find();
		}
                return $this->rates;
        }

	public function addAmount($c = 0)
	{
		$this->rates += ceil( $c);
	}

        public function addAmountWithSave($c = 0)
        {
                $this->balance += ceil( $c);
		$this->save();
        }

	//@todo  trigger for rates in negative values boundery

}
