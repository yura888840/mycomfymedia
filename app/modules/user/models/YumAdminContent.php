<?

/**
 * This is the model class for a AdminContent in Yum
 *
 * 
 */
class YumAdminContent extends YumActiveRecord
{

	public $is_active = 0;
	public $is_new = 0;
	public $is_approved = 0;
	public $Name, $data, $Id, $file_id;

	public function tableName()
        {
                return 'files';
        }


	// ???
/*	public function behaviors()
	{
		return array(
				'CAdvancedArBehavior' => array(
					'class' => 'application.modules.user.components.CAdvancedArBehavior'));
	}*/

	public function setAttributes(/*$values*/$a, $safeOnly = true) {
		foreach($a as $k => $v) $this->$k = $v;
		parent::setAttributes($a);
	}

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}


	public function afterDelete()
	{

		return parent::afterDelete();
	}


	public function getData() {
		$db = $this->getDbConnection();
		$query = "
select f.*, u.username, pf.email, pf.firstname, pf.lastname, i.data  from files f left join user u on (u.id= f.user_id) left join profile pf on (pf.user_id = u.id)  left join image i on (i.file_id= f.id) order by f.`timestamp` desc
		";

		return $db->createCommand($query)->queryAll();

	}

	public function search()
	{
		
		$criteria = new CDbCriteria;
		$criteria->addCondition('is_new=1');

		return new CActiveDataProvider(get_class($this), array(
					'criteria' => $criteria,
					'pagination' => array('pageSize' => Yum::module()->pageSize),
					));

		

		
	}


	public function beforeValidate()
	{
		if ($this->isNewRecord) {

		}

		return true;
	}

	public function afterSave()
	{
		if ($this->isNewRecord) {

		}

		return parent::afterSave();
	}

	public function rules()
	{
		return array();
	}


	// possible relations are cached because they depend on the active submodules
	// and it takes many expensive milliseconds to evaluate them all the time
	public function relations()
	{ 
		return array(
			'user' => array(self::BELONGS_TO, 'YumUser', 'user_id'),
			'image'=> array(self::HAS_ONE, 'Image', 'file_id'),
		);
	}


/// переделать scopes
/*
	public function scopes()
	{
		return array(
				'active' => array('condition' => 'status=' . self::STATUS_ACTIVE,),
				'inactive' => array('condition' => 'status=' . self::STATUS_INACTIVE,),
				'banned' => array('condition' => 'status=' . self::STATUS_BANNED,),
				'superuser' => array('condition' => 'superuser = 1',),
				'public' => array(
					'join' => 'LEFT JOIN privacysetting on t.id = privacysetting.user_id',
					'condition' => 'appear_in_search = 1',),
				);
	}*/

}
