<?

/**
 *
 * 
 */

class YumPaymentPB extends YumPayment
{

	protected $params;

	// @todo validator for order_id, amount
	public function __construct($data = array() ) {
		if(!empty($data) && is_array($data))
			$this->params = $data;

	}

	public function getDetails() {
	        $Version='1.0.0';

		$this->params['amount'] = $this->reformatAmount($this->params['amount']);

		$this->params['order_id'] = array_key_exists('order_id', $this->params)? 
                                                $this->params['order_id']: 
                                                $Yii::app()->user->data()-> id . strtotime('now');

		$data = array(
			'url' => Yii::app()->params['payments']['pb']['url'],
			'responseUrl' => Yii::app()->params['payments']['pb']['responseUrl'],
			'Version' => $Version,
			'OrderID' => $this->params['order_id'],
			'MerID' => Yii::app()->params['payments']['pb']['merchantId'],
			'AcqID' => Yii::app()->params['payments']['pb']['aquireId'],
			'PurchaseAmt' => $this->params['amount'],
			'PurchaseCurrencyExponent' => Yii::app()->params['payments']['pb']['purchaseCurrencyExponent'],
			'PurchaseCurrency' => Yii::app()->params['payments']['pb']['purchaseCurrency'],
			'OrderDescription' => $this->params['desc'],
			'Signature' => $this->buildSignature(),
		);//var_dump($data);die();

		return $data;
	}

    private function reformatAmount($amount) {
        $amount = $amount * 100;
        $amount = str_pad($amount, 12, '0', STR_PAD_LEFT);

        return $amount;
    }

	private function buildSignature() {
                $str =  Yii::app()->params['pb']['password'].
			Yii::app()->params['pb']['merchantId'].
			Yii::app()->params['pb']['aquireId'].
			$this->params['order_id'].
			$this->params['amount'].
			Yii::app()->params['pb']['purchaseCurrency'].
			$this->params['desc'];

                $signature=sha1($str);
                $signature=$this->hexbin($signature);
                $signature = base64_encode($signature);

		return $signature;
	}

    private function hexbin($temp) {
       $data="";
       $len = strlen($temp);
       for ($i=0;$i<$len;$i+=2) $data.=chr(hexdec(substr($temp,$i,2)));
       return $data;
    }

}
