<?php

class MembershipPrices {

	const MEMBERSHIP_STANDART = 0;
	const MEMBERSHIP_SILVER = 1;
	const MEMBERSHIP_GOLD = 2;

	private static $values_text = [
		"Standart membership",
		"Silver Membership",
		"Gold Membership"
	];

	private static $values = array(
		1,//50,
		1,//100,
		1,//150,
	);

	public static $default_price = 50;
	public static $default_text = "Balance charge";

	public static function getPriceFor($memrshp_type) {
		return array_key_exists($memrshp_type, self::$values)
			? self::$values[$memrshp_type ]
			: self::$default_price;
	}

	public static function getTextFor($memrshp_type) {
		return array_key_exists($memrshp_type, self::$values_text)
			? self::$values_text[$memrshp_type ]
			: self::$default_text;
	}
} 
