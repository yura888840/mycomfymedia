<?php

/**
 * This is the model class for the Payment in Yum
 *
 * 
 */

class YumPaymentResponsePrivat24 extends YumPaymentResponse
{
	// params to parse from responses
	protected $params = array(

	);

	public function __construct($data = array()) {

		parent::__construct($data);
	}

	private $paysys_currency = 'UAH';

	// вход:
	//  date
	//  amt
	// 
	public function loadBalances($_data) {
			$_payment = $_data;

                        $_payment['date'] = gmdate('U');//substr($_payment['date'],0, -2);
                        $amount = str_replace( ',', '.', $_payment['amt']) ;
                        $ts = gmdate('U');//date('Y-m-d H:i:s', $_payment['date'] );
			if(!array_key_exists('user_id', $_data))
                        	$user_id = Yii::app() ->user->data() ->id;
			else $user_id = $_data['user_id'];

                        $balance = YumBalance::model();
                        $balance->user_id = $user_id;

                        $balance->balance = $amount;
                        $balance->timestamp = $ts;
                        $balance->scenario = 'insert';
                        $balance->isNewRecord = true;
                          if($balance->validate()){
                            $balance->save(false);
                          }
                          else{
                              ///echo CHtml::errorSummary($balance);
			      //  проверить, вот эт О
                          }
		
                        //if( $balance->save() )
                        //      $state = true ;
                        //else  
                        
                        //////$order = YumOrder::model() ->findByAttributes(array('data' => $_payment['order']));
                        // order - создаем, до начала платежа (верней, в момент, когда идем, идет переход, к плат. с- ме, шлюзу )
                        
                        $payment = YumPayments::model();
                        $payment->user_id = $user_id;
                        $payment->order_id = $_payment['order_id_mycomfy'];
                        $payment->amount = $_payment['amt'];
                        $payment->paydate = $_payment['date'];
                        $payment->direction = 'payin';
                        $payment->gateway = 1; // hardcoded code of Privat24
                        $payment->is_approved = 1;
                        $payment->isNewRecord = true;
                        $payment->save();
			//  эту связку, проРаботать
			
                        
                        $crAmount = YumCreditsForMoney::calcRates($_payment['amt'], $this->paysys_currency);
                        $cr = YumRatesBalance::model()->findByAttributes(array('user_id' => $user_id) );

			if(empty($cr)) {
				$cr = YumRatesBalance::model(); 
				$cr->user_id = $user_id; 
				$cr->isNewRecord = true;
			}

                        $cr->balance = $crAmount + (empty($cr->balance) ? 0 : $cr->balance);
                        $cr->validate(); $cr->save();
                        
                        $state = true;

	}

	public function parse() {
		$state = $_payment = false;
		if(!empty($this->data['payment'])) {
			$_p = explode('&', $this->data['payment']);

			$_payment = array();

			foreach($_p as $v) {
				$d = explode('=', $v);
				$_payment[$d[0]] = $d[1];
			}
		}
		// проверку, пар - р  ров втулить  ..
		if($_payment && $_payment['state'] == 'ok' ) {
			$_payment['order_id_mycomfy'] = $this->data['order_id_mycomfy'];
			$this->loadBalances($_payment);

			$state = true;
		}

		$msg = 'Your balance has been loaded for ..';
		return array('success' => $state, 'items' => $msg);

	}

	const defaul_currency = 'UAH';

	private function checkSignature($sign ) {

		return true;
	}

}
