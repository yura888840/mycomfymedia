<?php

/**
 * This is the model class for the Payment in Yum
 *
 */

interface ResponsePayment {

	public function chargeBalance();

	public function logFailPayment();

	//public function 
}

class YumPaymentResponse extends CFormModel
{

	protected $money;
	protected $currency;

	protected $userId;

	protected $data = array();

	public function __construct($data = array()) {
		if(!empty($data)) $this->data = $data;
	}

	public function parse() {
		return false;
	}

	public function chargeBalance() {
		$balance = YumBalance::model();

		$balance->user_id = $this->userId;
		$balance->balance = $this->amount;

		$balance->currency = $this->currency;
		$balance->scenario = 'insert';
		$balance->isNewRecord = true;
		if($balance->validate()){
			$balance->save(false);
		} else {
			echo CHtml::errorSummary($balance);
			throw new Exception('Smthing wrong');
		}
		
	}

	public function logFailPayment() {

	}

}
