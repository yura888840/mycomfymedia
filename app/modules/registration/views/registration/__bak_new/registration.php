<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="icon" type="image/x-icon" href="/favicon.ico">

    <!-- the styles -->
    <link href="/css/xvid.css" rel="stylesheet">
    <link href="/css/mediabox/mediabox.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="/js/html5.js"></script>
    <![endif]-->

    <!--[if lt IE 8]>
    <script src="/js/json2.js"></script>
    <![endif]-->
    <title>My Comfy MediaMarket</title>

    <!-- {{ App.bootstrap.register() }} -->
    <script type="text/javascript" src="/js/libs/jquery.cookie.js"></script>
<script type="text/javascript">
function getCookie(name) {
  var value = "; " + document.cookie;
  var parts = value.split("; " + name + "=");
  if (parts.length == 2) return parts.pop().split(";").shift();
} </script>
</head>
<body>

<div id="xvidbar" class="metanav navbar">
    <div class="navbar-inner">
        <div class="container">

            <ul class="nav pull-right">
                    <li>
                        <a href="/">Home</a>
                    </li>
                    <li>
                        <a href="/about">Help</a>
                    </li>
            </ul>
        </div>
    </div>
</div>

<div class="page">

<nav id="mainnav">
<div class="container">
<h1 class="xvidMediaBox_25px h1logo"><a href="/">MyComfyMediaMarket</a></h1>
</div>
</nav>

<div class="form container">
<div style="padding: 20px 0 0 20px;">


<h2> <?php echo Yum::t('Registration'); ?> </h2>

<?php $this->breadcrumbs = array(Yum::t('Registration')); ?>

<div class="form">
<?php $activeform = $this->beginWidget('CActiveForm', array(
                        'id'=>'registration-form',
                        'enableAjaxValidation'=>true,
                        'enableClientValidation'=>true,
                        'focus'=>array($form,'username'),
                        ));
?>

<?php echo Yum::requiredFieldNote(); ?>
<?php echo CHtml::errorSummary(array($form, $profile)); ?>

<div class="row"> <?
echo $activeform->labelEx($form,'username');
echo $activeform->textField($form,'username');
?> </div>

<div class="row"> <?
echo $activeform->labelEx($profile,'email');
echo $activeform->textField($profile,'email');
?> </div>

<div class="row"> <?
echo $activeform->labelEx($profile,'firstname');
echo $activeform->textField($profile,'firstname');
?> </div>

<div class="row"> <?
echo $activeform->labelEx($profile,'lastname');
echo $activeform->textField($profile,'lastname');
?> </div>

<div class="row">
<?php echo $activeform->labelEx($form,'password'); ?>
<?php echo $activeform->passwordField($form,'password'); ?>
</div>

<div class="row">
<?php echo $activeform->labelEx($form,'verifyPassword'); ?>
<?php echo $activeform->passwordField($form,'verifyPassword'); ?>
</div>

<?php if(extension_loaded('gd')
                        && Yum::module('registration')->enableCaptcha): ?>
        <div class="row">
                <?php echo CHtml::activeLabelEx($form,'verifyCode'); ?>
                <div>
                <?php $this->widget('CCaptcha'); ?>
                <?php echo CHtml::activeTextField($form,'verifyCode'); ?>
                </div>
                <p class="hint">
                <?php echo Yum::t('Please enter the letters as they are shown in the image above.'); ?>
                <br/><?php echo Yum::t('Letters are not case-sensitive.'); ?></p>
        </div>
        <?php endif; ?>

        <div class="row submit">
                <?php echo CHtml::submitButton(Yum::t('Registration')); ?>
        </div>



<?php $this->endWidget(); ?>
</div><!-- form -->

</div>
</div>

</div>

<footer id="pageFooter" class="container">
    MyComfyMediaMarket 2014
</footer>

</body>
</html>

