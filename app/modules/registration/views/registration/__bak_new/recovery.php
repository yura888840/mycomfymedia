<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="icon" type="image/x-icon" href="/favicon.ico">

    <!-- the styles -->
    <link href="/css/xvid.css" rel="stylesheet">
    <link href="/css/mediabox/mediabox.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="/js/html5.js"></script>
    <![endif]-->

    <!--[if lt IE 8]>
    <script src="/js/json2.js"></script>
    <![endif]-->
    <title>My Comfy MediaMarket</title>

    <!-- {{ App.bootstrap.register() }} -->
    <script type="text/javascript" src="/js/libs/jquery.cookie.js"></script>
<script type="text/javascript">
function getCookie(name) {
  var value = "; " + document.cookie;
  var parts = value.split("; " + name + "=");
  if (parts.length == 2) return parts.pop().split(";").shift();
} </script>
</head>
<body>

<div id="xvidbar" class="metanav navbar">
    <div class="navbar-inner">
        <div class="container">

            <ul class="nav pull-right">
                    <li>
                        <a href="/">Home</a>
                    </li>
                    <li>
                        <a href="/about">Help</a>
                    </li>
            </ul>
        </div>
    </div>
</div>

<div class="page">

<nav id="mainnav">
<div class="container">
<h1 class="xvidMediaBox_25px h1logo">Xvid MediaBox</h1>
</div>
</nav>

<div class="form container">
<div style="padding: 20px 0 0 20px;">

<?php
$this->pageTitle = Yum::t('Password recovery');

$this->breadcrumbs=array(
        Yum::t('Login') => Yum::module()->loginUrl,
        Yum::t('Restore'));

?>
<?php if(Yum::hasFlash()) {
echo '<div class="success">';
echo Yum::getFlash();
echo '</div>';
} else {
echo '<h2>'.Yum::t('Password recovery').'</h2>';
?>

<div class="form">
<?php echo CHtml::beginForm(); ?>

        <?php echo CHtml::errorSummary($form); ?>

        <div class="row">
                <?php echo CHtml::activeLabel($form,'login_or_email'); ?>
                <?php echo CHtml::activeTextField($form,'login_or_email') ?>
                <?php echo CHtml::error($form,'login_or_email'); ?>
                <p class="hint"><?php echo Yum::t("Please enter your user name or email address."); ?></p>
        </div>

        <div class="row submit">
                <?php echo CHtml::submitButton(Yum::t('Restore')); ?>
        </div>

<?php echo CHtml::endForm(); ?>
</div><!-- form -->
<?php } ?>


</div>
</div>

</div>

<footer id="pageFooter" class="container">
    MyComfyMediaMarket 2014
</footer>

</body>
</html>

