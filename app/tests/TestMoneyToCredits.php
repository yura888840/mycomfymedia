<?php

require_once(__DIR__ . '/../modules/user/models/YumCreditsForMoney.php');

class TestMoneyToCredits extends CTestCase
{

	public function setUp()
	{
		parent::setUp();
	}

	public function tearDown()
	{

		parent::tearDown();
	}

        public function testConvertUSDToCreditsLowBoundRange0()
        {
                $sum = 1; $cur = 'USD'; $estimated = 2;
                $credit_num = YumCreditsForMoney::calcRates($sum, $cur);
                $this->assertEquals($credit_num, $estimated);

        }

        public function testConvertUSDToCreditsBoundRange0()
        {
                $sum = 29; $cur = 'USD'; $estimated = ceil(29 * 1.4);
                $credit_num = YumCreditsForMoney::calcRates($sum, $cur);
                $this->assertEquals($credit_num, $estimated);

        }

        public function testConvertUSDToCreditsBoundRange1()
        {
                $sum = 30; $cur = 'USD'; $estimated = 30 * 1.8;
                $credit_num = YumCreditsForMoney::calcRates($sum, $cur);
                $this->assertEquals($credit_num, $estimated);

        }

	public function testConvertUSDToCredits()
	{
                $sum = 50; $cur = 'USD'; $estimated = 1.8 * 50;
                $credit_num = YumCreditsForMoney::calcRates($sum, $cur); 
                $this->assertEquals($credit_num, $estimated);

	}// 'usd'

        public function testConvertUSDToCredits500()
        {
                $sum = 500; $cur = 'USD'; $estimated = 2.5 * 500;
                $credit_num = YumCreditsForMoney::calcRates($sum, $cur);
                $this->assertEquals($credit_num, $estimated);

        }

        public function testConvertUSDToCredits499()
        {
                $sum = 499; $cur = 'USD'; $estimated = ceil(2.2 * 499);
                $credit_num = YumCreditsForMoney::calcRates($sum, $cur);
                $this->assertEquals($credit_num, $estimated);

        }

} 
