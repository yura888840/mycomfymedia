<?php

/**
 * This is the component class for basket items using Redis.
 *
 */
class BasketItems
{

	private $client;

	private $hashSet;

	private $hash;

	const COUNT_PREFIX = "count_";

	public function __construct()
	{
		$this->client = Yii::app()->redis->getClient();
		$user_id = Yii::app()->user->data()->id;
		$basket = Basket::model()->findByAttributes(array('user_id' => $user_id));

	        if(!$basket) 
	        {
			$ts = time(null);
	                $basket = Basket::model();
	                $basket->user_id = $user_id;
	                $basket->hash = $user_id . $ts;
	                $basket->isNewRecord = true;
	                if($basket->validate())
	                        $basket->save();
	        }
		$this->hash = $basket->hash;

		$this->hashSet = new ARedisHash($this->hash);
	}

	public function addItem($scu, $data)
	{
		$this->hashSet[$scu] = serialize($data);
		$this->incItemCount($scu );
		return true;
	}

	public function removeItems($sku)
	{
		$this->client->hDel($this->hash, $sku);
		$this->client->hDel($this->hash, self::COUNT_PREFIX . $sku);

		return true;
	}

	public function incItemCount($sku)
	{
		$this->client->hIncrBy($this->hash, self::COUNT_PREFIX . $sku, 1);
		return true;
	}

        public function decItemCount($sku)
        {
                $this->client->hIncrBy($this->hash, self::COUNT_PREFIX . $sku, -1);
		if($this->hashSet[self::COUNT_PREFIX . $sku] < 1)
			$this->removeItems($sku);

                return true;
        }

	public function removeAllFor()
	{
		$this->client->del($this->hash);
	}

	public function getTotalAmount()
	{

		return 1;
	}

	public function getAllItems()
	{
		return $this->client->hGetAll($this->hash);
	}
}
