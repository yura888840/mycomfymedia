<?php

class SModel extends CActiveRecord {

	public function defaultScope() {

		return array( 
			'condition' => "user_id=" . intval(Yii::app()-> user->data()-> id)
		);

	}

	public function hasAccess() {
		if(!$this->user_id) return true; // ???

		$user_id = Yii::app()->user-> data()->id;

		if($this->user_id == $user_id || $this->isAdmin())
			return true;

		return false;

	}

	public function isAdmin() {
		return true;
	}

	protected function afterFind() {

		/*if(!$this->hasAccess() )
			foreach(class_proteprties($this) as $prop => $val)
			  $this->{$prop} = null;*/

		parent::afterFind();
	}

	protected function beforeFind() {
		if(Yii::app()->user-> data()->id)
			$this->user_id = Yii::app()->user-> data()->id;
		parent::beforeFind();
	}
}
